/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

import Vue from 'vue/dist/vue.esm.js';

//import vuetify from './plugins/vuetify'; // path to vuetify export
import Vuetify from 'vuetify';
import VuetifyMoney from "vuetify-money";
Vue.use(VuetifyMoney);

Vue.use(Vuetify, {
});
import Calculator from './calculator/calculator.vue';

var app = new Vue({

    el: '#calculator-app',
    components: {
        'calculator': Calculator

    },
    vuetify: new Vuetify(),
    methods: {

    }

});
