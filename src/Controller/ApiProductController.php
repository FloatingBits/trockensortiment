<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use JMS\Serializer\SerializerInterface;
use JMS\SerializerBundle\JMSSerializerBundle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/product")
 */
class ApiProductController extends AbstractController
{
    /**
     * @Route("/", name="api_product_index", methods={"GET"})
     */
    public function index(ProductRepository $productRepository, SerializerInterface $serializer ): Response
    {

        $products = $productRepository->findAll();
        $data = $serializer->serialize($products, 'json');

        return new JsonResponse($data, 200, [], true);
    }

}
