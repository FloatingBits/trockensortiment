<?php

namespace App\Import;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ExcelImport {

    const GENERAL_SHEET_TITLE = "Tabelle1";
    const NAGEL_SHEET_TITLE = "Tofu Nagel";

    const ID_PACKAGING = 'Packung';
    const ID_GLASS = 'Glas';
    const ID_GRAM = '100g';
    const ID_BOTTLE = 'Flasche';
    const ID_TAFEL = 'Tafel';
    const ID_ML= '100ml';
    const ID_ML2= '100 ml';
    const ID_NAGEL_TITLE_INDEX = 'A';
    const ID_NAGEL_PRICE_INDEX = 'B';
    const ID_NAGEL_EXCLUDE_TITLE = 'Stand:';
    const ID_GENERAL_TITLE_INDEX = 'B';
    const ID_GENERAL_UNIT_INDEX = 'D';
    const ID_GENERAL_PRICE_INDEX = 'E';
    const ID_GENERAL_PRICE_PER_KG_INDEX = 'G';

    const ALLOWED_PACKAGING_IDENTIFIERS = [
        self::ID_PACKAGING,
        self::ID_BOTTLE,
        self::ID_GLASS,
        self::ID_GRAM,
        self::ID_TAFEL,
        self::ID_ML,
        self::ID_ML2
    ];

    /**
     * @var EntityManagerInterface
     */
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function import($fileName) {
        $reader = new Xlsx();
        $spreadSheet = $reader->load($fileName);
        $sheets = $spreadSheet->getAllSheets();
        $repo = $this->em->getRepository(Product::class);
        $this->clearProducts();
        foreach ($sheets as $sheet) {
            switch ($sheet->getTitle()) {
                case self::GENERAL_SHEET_TITLE:
                    $this->importGeneralSheet($sheet);
                    break;
                case self::NAGEL_SHEET_TITLE:
                    $this->importNagelSheet($sheet);
                    break;
            }
            $sheet->getTitle();
        }
        //var_dump($xlsx->listWorksheetNames($file->getPathName()));exit;
    }

    private function clearProducts() {
        $q = $this->em->createQuery('delete from ' . Product::class . ' p where p.isImported=1 ');
        $numDeleted = $q->execute();
    }

    private function importGeneralSheet(Worksheet $sheet) {

        $rows = $sheet->getRowIterator();
        foreach ($rows as $row) {
            $rowIndex = $row->getRowIndex();
            $cellId = self::ID_GENERAL_UNIT_INDEX . $rowIndex;
            $unit = $sheet->getCell($cellId)->getValue();
            if (in_array($unit, self::ALLOWED_PACKAGING_IDENTIFIERS)) {
                $product = new Product();
                $product->setTitle($sheet->getCell(self::ID_GENERAL_TITLE_INDEX . $rowIndex)->getValue());
                $priceIndex = ($unit == self::ID_GRAM || $unit == self::ID_ML || $unit == self::ID_ML2) ? self::ID_GENERAL_PRICE_PER_KG_INDEX : self::ID_GENERAL_PRICE_INDEX;
                $price = $this->parsePrice($sheet->getCell($priceIndex . $rowIndex)->getValue());
                if ($unit == self::ID_GRAM) {
                    //We're reading the KG price here!
                    $price = $price/1000;
                    $unit = 'g';
                }
                if ($unit == self::ID_ML || $unit == self::ID_ML2) {
                    //We're reading the KG price here!
                    $price = $price/1000;
                    $unit = 'ml';
                }
                $product->setPricePerUnit($price);
                $product->setUnit($unit);
                $product->setProvider('?');
                $product->setExternalId('?_' . $rowIndex);
                $product->setSearchTerms('');
                $product->setIsImported(true);
                $this->em->persist($product);
            }
        }

        $this->em->flush();
    }

    private function importNagelSheet(Worksheet $sheet) {
        $rows = $sheet->getRowIterator();
        foreach ($rows as $row) {
            $rowIndex = $row->getRowIndex();
            $price = $sheet->getCell(self::ID_NAGEL_PRICE_INDEX . $rowIndex)->getValue();
            $title = $sheet->getCell(self::ID_NAGEL_TITLE_INDEX . $rowIndex)->getValue();
            if (strpos(self::ID_NAGEL_EXCLUDE_TITLE, $title) === false &&  strlen($price)) {
                $product = new Product();
                $product->setTitle($title);
                $product->setPricePerUnit($price);
                $product->setUnit('Packung');
                $product->setProvider('?');
                $product->setExternalId('?_' . $rowIndex);
                $product->setSearchTerms('');
                $product->setIsImported(true);
                $this->em->persist($product);
            }
        }

        $this->em->flush();
    }

    private function parsePrice($priceString) {
        // convert "," to "."
        $s = str_replace(',', '.', $priceString);

        // remove everything except numbers and dot "."
        $s = preg_replace("/[^0-9\.]/", "", $s);

        // remove all seperators from first part and keep the end
        $s = str_replace('.', '',substr($s, 0, -3)) . substr($s, -3);

        // return float
        return (float) $s;
    }
}