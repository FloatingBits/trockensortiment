
module.exports = function(grunt) {
    const heJson = grunt.file.exists('he.json') ? grunt.file.readJSON('he.json') : {};

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),


        sshconfig: {
            "he": heJson
        },
        sshexec: {
            gitPull: {
                command: [
                    'sh -c "cd <%=sshconfig.he.htdocsDir%>; git checkout -f; git pull;"'
                ],
                options: {
                    config: 'he'
                }
            },

            composer: {
                command: [
                    'sh -c "cd <%=sshconfig.he.htdocsDir%>; /usr/bin/php ~/bin/composer.phar install;"'
                ],
                options: {
                    config: 'he'
                }
            },
            updateSchema: {
                command: [
                    'sh -c "cd <%=sshconfig.he.htdocsDir%>; /usr/bin/php bin/console doctrine:schema:update --force --env=stage;"'
                ],
                options: {
                    config: 'he'
                }
            }
        },
        sftp: {
            assets: {
                files: {
                    "./": "public/build/**"
                },
                options: {
                    path: "<%=sshconfig.he.htdocsDir%>",
                    createDirectories: true,
                    config: 'he'
                }

            }
        }


        //Add the Tasks configurations here.

    });

    grunt.loadNpmTasks('grunt-ssh');

// Define Tasks here

    grunt.registerTask('deploy', ['sshexec:gitPull', 'sshexec:composer', 'sshexec:updateSchema', 'sftp:assets']);

};